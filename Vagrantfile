# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure("2") do |config|
  config.vm.define "lxdhost" do |lxdhost|
    lxdhost.vm.box = "truemark/ubuntu-20.04-basic"

    # https://www.vagrantup.com/docs/networking/private_network.html
    #lxdhost.vm.network "private_network", ip: "192.168.33.20", :netmask => "255.255.255.0"
    config.vm.network "public_network",
       use_dhcp_assigned_default_route: true
#     lxdhost.vm.network "public_network", ip: "192.168.169.30"
    lxdhost.vm.hostname = "lxdhost"

    # Add additional vmdk file
    lxdhost.vm.provider "vmware_fusion" do |vm, override|
      vdiskmanager = '/Applications/VMware\ Fusion.app/Contents/Library/vmware-vdiskmanager'
      dir = Dir.pwd + "/.vagrant/additional-disk"
      unless File.directory?( dir )
        Dir.mkdir dir
      end
      file_to_disk = "#{dir}/lxdhost-zfs.vmdk"
      unless File.exists?( file_to_disk )
        `#{vdiskmanager} -c -s 50GB -a lsilogic -t 1 #{file_to_disk}`
      end
      vm.vmx['scsi0:1.filename'] = file_to_disk
      vm.vmx['scsi0:1.present']  = 'TRUE'
      vm.vmx['scsi0:1.redo']     = ''
    end

    # Delete vmdk files after destruction
    config.trigger.after :destroy do |t|
      dir = Dir.pwd + "/.vagrant/additional-disk"
      t.run = {inline: "bash -c 'rm -rf #{dir}/lxdhost*'"}
    end

  end

  # Show GUI instead of run headless
#   config.vm.provider "vmware_fusion" do |v, override|
#     v.gui = true
#   end

  # Configure CPU and Memory
  # https://www.vagrantup.com/docs/providers/configuration.html
  config.vm.provider "vmware_fusion" do |v, override|
    v.vmx["memsize"] = "4096"
    v.vmx["numvcpus"] = "2"
  end

  # Share folder
  config.vm.synced_folder ".", "/srv/salt"

  # Execute bootstrap
  config.vm.provision "shell", privileged: true, inline: <<-SHELL
    /srv/salt/bootstrap.sh
  SHELL
end
