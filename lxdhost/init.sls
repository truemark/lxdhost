dependencies:
  pkg.installed:
    - pkgs:
      - apparmor
      - bridge-utils
      - dnsmasq
      - iptables
      - iptables-persistent
      - lxd
      - lxd-client
      - lxd-tools
      - python3-pylxd
      - screen
      - tmux
      - zfsutils-linux


'/etc/issue':
  file.rename:
    - source: /etc/issue.original
    - force: true

net.ipv4.ip_forward:
  sysctl.present:
    - value: 1

zpool:
  cmd.run:
    - name: zpool create storage sdb
    - unless:
      - zpool list storage
    - require:
      - dependencies

'zpool set autoexpand=on storage':
  cmd.run:
    - unless:
      - "[[ $(zpool get autoexpand storage | grep autoexpand | awk '{print $3}') == 'on' ]]"
    - require:
      - zpool

initialize:
  lxd.init:
    - storage_backend: zfs
    - storage_pool: storage
    - require:
      - zpool
